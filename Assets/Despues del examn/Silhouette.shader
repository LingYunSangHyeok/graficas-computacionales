﻿Shader "Custom/Silhouette" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		//producto punto de física
		//producto punto obtiene las vistas ortogonals para saber a ue caras dibujar auellas silueta
		_DotProduct("Rim effect", Range(-1,1))=.25


	}
	SubShader {
		Tags 
		{
		"RenderType"="Transparent" 
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		}
		LOD 200
		
		CGPROGRAM
		//Lambert es más ligero más viejo y con menos posibilidades
		//fade combina lo ue está atrás y el material Transparente
		#pragma surface surf Lambert alpha:fade


		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			//te da las normals con respecto a la posicion del mundo 
			float3 worldNormal;
			//respecto a la cámara
			float3 viewDir;
		};

		fixed4 _Color;
		float _DotProduct;



		void surf (Input IN, inout SurfaceOutput o) 
		{
			float4 c = tex2D(_MainTex,IN.uv_MainTex)* _Color;
			o.Albedo = c.rgb;
			// Dot es la funcion ue calcula el producto punto
			float border = 1- (abs(dot(IN.viewDir,IN.worldNormal))); 
			//alpha, para ver ue tanto se va a difuminar el modelo hacia adentro del holograma
			float alpha = (border *(1-_DotProduct)+ _DotProduct);
			o.Alpha = c.a * alpha;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

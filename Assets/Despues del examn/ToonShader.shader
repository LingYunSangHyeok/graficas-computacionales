﻿Shader "Custom/ToonShader" {
	Properties {
	//necesitamos maintex y Ramp ue es como degradado pero muy marcado
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_RampTex("Ramp", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		//Toon es mi modelo de iluminación
		#pragma surface surf Toon
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _RampTex;

		struct Input 
		{
			float2 uv_MainTex;
		};


		half4 LightingToon(SurfaceOutput s, fixed3 LightDir, fixed3 atten)
		{
				//Esto es la reflexion
			half NdotL = dot(s.Normal, LightDir);
			/*esto es para el snap*/
			NdotL = tex2D(_RampTex, fixed2(NdotL, 0.5 ));
			half4 c; 
			c.rgb = s.Albedo * _LightColor0.rgb * NdotL * atten ;
			c.a = s.Alpha;
			return c;
		}
		void surf (Input IN, inout SurfaceOutput o) 
		{
			o.Albedo = tex2D(_MainTex,IN.uv_MainTex).rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/Specular" {
	Properties {
	//PHONG Specular
		_MainTint ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SpecularColor("SpecularColor", color) = (1,1,1,1)
		_SpecPower("Specular power", Range(1,30)) = 1
		}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Phong
		#pragma target 3.0

		sampler2D _MainTex;
		float4 _SpecularColor;
		float4 _MainTint;
		float _SpecPower;

		struct Input 
		{
			float2 uv_MainTex;
		};

		fixed4 LightingPhong(SurfaceOutput s, fixed3 LightDir, half3 viewDir,fixed atten)
		{ 
			float NdotL = dot(s.Normal, LightDir);
			//normalize hace al vector unitairo y deja la direccion
			float3 reflectionVector = normalize(2.0 * s.Normal * NdotL - LightDir);
			float spec = pow(max(0,dot(reflectionVector,viewDir)),_SpecPower);
			float3 finalspec = _SpecularColor.rgb *spec; 
			fixed4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * max(0,NdotL)*atten)+(_LightColor0.rgb * finalspec);
			c.a = s.Alpha;
			return c;
		}
		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D(_MainTex,IN.uv_MainTex)*_MainTint;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/LambertReflection" {
	Properties {
	//Solo necesita txtura
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
						//el nombre inicia en lighting pero no se escribe aui abajo
						//usa mi modelo de iluminacion
		#pragma surface surf SimpleLambert
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		};
		//nuevo modeelo de iluminación versiion simplificada dle lambert
		half4 LightingSimpleLambert(SurfaceOutput s, half3 LightDir, half atten) //half3 para saber a ue lugar apunta la funcion
		{
			half NdotL = dot(s.Normal, LightDir);
			half4 c;
			c.rgb = s.Albedo *  _LightColor0.rgb * (NdotL * atten); //atenuacion = atten
			c.a = s.Alpha;
			return c;
		}	

		//surf aun es necesaria
		//borra el standard
		void surf (Input IN, inout SurfaceOutput o) 
		{
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/Transparent" {
	Properties 
	{
	//sin glosi ni metalic
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		//El Tag es importante
		//tag le dice al gpu como interpretar
		Tags 
		{
		//Transparente porue queremos lograr transparencia
		"RenderType"="Transparent" 
		//unity tiene niveles de render 
		//lo primero ue se dibuja es lo ue está más atras
		//con esta cola le decimos a cada cosa se dibuje
		//si uereemos ue sea primero lo mandamos hasta el fondo

		//Render ueue de unity BUSCAR
		"Queue" = "Transparent"
		"IgnoreProjector"= "True"
		}

		LOD 200		
		CGPROGRAM
		//Transparente no necesita gnrar sombras /*fullforwardshadows*/
		#pragma surface surf Standard alpha:fade
		#pragma target 3.0


		sampler2D _MainTex;


		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
		float4 c = tex2D(_MainTex,IN.uv_MainTex)* _Color;
		o.Albedo = c.rgb;
		o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/Examen_NormalMap" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap ("normalmap", 2D) = "bump" {}
	    _NormalMapIntensity("Normalmap Intensity", Range (0,5)) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
	    float _NormalMapIntensity;
		fixed4 _Color;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};
		
		void surf (Input IN, inout SurfaceOutput o) 
		{	
			o.Albedo = tex2D (_MainTex,IN.uv_MainTex).rgb;
			float3 map = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
			map.x *= _NormalMapIntensity;
			map.y *= _NormalMapIntensity;
			o.Normal = normalize(map);
		}
		ENDCG
	}
	FallBack "Diffuse"
}

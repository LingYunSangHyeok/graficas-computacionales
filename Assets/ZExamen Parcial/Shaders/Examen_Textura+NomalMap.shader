﻿Shader "Custom/Examen_Textura+NomalMap" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		_NormalMap ("normalmap", 2D) = "bump" {}

		_ScrollXSpeed("X Scroll Speed", Range(0,10)) = 2
		_ScrollYSpeed("Y Scroll Speed",Range(0,10)) = 0


	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows
		#pragma target 3.0		

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};

		sampler2D _MainTex;
		sampler2D _NormalMap;
		fixed4 _Color;


		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;


		void surf (Input IN, inout SurfaceOutput o) 
		{
			        o.Albedo = tex2D (_MainTex,IN.uv_MainTex).rgb;
					float3 Mapa = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
					o.Normal = normalize(Mapa);

					fixed2 ScrolledUV = IN.uv_MainTex;
					fixed xScrollValue = _ScrollXSpeed * _Time;
					fixed yScrollValue = _ScrollYSpeed * _Time;
					ScrolledUV += fixed2 (xScrollValue, yScrollValue);
					half4 c = tex2D(_MainTex,ScrolledUV);
					o.Albedo = c.bgr*_Color;
					o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

﻿Shader "Custom/Examen_Textured" {
	Properties {
		//No necesitamos Glossiness ni Metallic
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ScrollXSpeed("X Scroll Speed", Range(1,1)) = 0
		_ScrollXSpeed("X Scroll Speed", Range(1,1)) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;
		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;



		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
		fixed2 ScrolledUV = IN.uv_MainTex;
		fixed xScrollValue = _ScrollXSpeed * _Time;
		fixed yScrollValue = _ScrollYSpeed * _Time;
		ScrolledUV += fixed2 (xScrollValue, yScrollValue);
		half4 c = tex2D(_MainTex,ScrolledUV);
		o.Albedo = c.rgb*_Color;
		o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

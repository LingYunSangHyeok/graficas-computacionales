﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movie : MonoBehaviour {
    
    public string folder = "Frames_ProyectoFinal";
    public int FPS = 25;
    public int Tamaño = 1;

    private string realFolder = "";

    void Start()
    {
        Time.captureFramerate = FPS;        
        realFolder = folder;
        int count = 1;
        while (System.IO.Directory.Exists(realFolder))
        {
            realFolder = folder + count;
            count++;
        }
        System.IO.Directory.CreateDirectory(realFolder);
    }

    void Update()
    {
        var name = string.Format("{0}/shot {1:D04}.png", realFolder, Time.frameCount);
        ScreenCapture.CaptureScreenshot(name, Tamaño);

    }
    
}

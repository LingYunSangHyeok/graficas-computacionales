﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour {

    Camera My_camara;
    float Temporizador;

	void Start ()
    {
        My_camara = GetComponent<Camera>();
        My_camara.transform.position = new  Vector3(-1, 1, -8);
	}
	
	void Update ()
    {
        Temporizador += Time.deltaTime;
        if (Temporizador >= 35)
        {
            My_camara.transform.position = Vector3.Lerp(transform.position, new Vector3(-1, 1, -7), Time.deltaTime/2);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExamenP02 : MonoBehaviour {

    float Temporizador;
    Renderer Myrend;
    Shader shader01, shader02, shader03, shader04;
    float step;
    string Textura;

    void Start()
    {
        Myrend = GetComponent<Renderer>();
        Myrend.material.SetColor("_Color", Color.black);
        shader01 = Shader.Find("Custom/Examen_Diffuse");
        shader02 = Shader.Find("Custom/Examen_Textured");
        shader03 = Shader.Find("Custom/Examen_NormalMap");
        shader04 = Shader.Find("Custom/Examen_Textura+NomalMap");
    }

    private void Update()
    {
        Temporizador += Time.deltaTime;
        Textura = (Myrend.material.mainTexture == null) ? "OFF" : "ON";
        if (Temporizador >= 10.0f)
        {
            Myrend.material.shader = shader01;
            Myrend.material.mainTexture = null;
            Myrend.material.SetColor("_Color", Color.LerpUnclamped(Myrend.material.color, Color.cyan, Time.deltaTime*2));
            Textura = (Myrend.material.mainTexture == null) ? "OFF" : "ON";
        }
        if (Temporizador >= 15.0f)
        {
            Myrend.material.SetColor("_Color", Color.white);
            Myrend.material.shader = shader02;            
            Myrend.material.mainTexture = Resources.Load("Examen_Textura01") as Texture;
            Textura = (Myrend.material.mainTexture == null) ? "OFF" : "ON";
        }
        if(Temporizador >= 20.0f)
        {
            step += Time.deltaTime / 2;
            Myrend.material.shader = shader03;
            Myrend.material.mainTexture = null;
            Myrend.material.SetTexture("_NormalMap", Resources.Load("Examen_NormalMap01") as Texture);
            Myrend.material.SetFloat("_NormalMapIntensity", (Mathf.Lerp(0, 5, step)));
            Myrend.material.SetTextureOffset("_NormalMap", new Vector2( Temporizador/10 ,0));
            Textura = (Myrend.material.mainTexture == null) ? "OFF" : "ON";
        }
        if(Temporizador >= 30.0f)
        {
            Myrend.material.shader = shader04;
            Myrend.material.SetTexture("_MainTex", Resources.Load("Examen_Textura02") as Texture);
            Myrend.material.SetTexture("_NormalMap", Resources.Load("Examen_NormalMap02") as Texture);
            Myrend.material.SetTextureOffset("_NormalMap", new Vector2(Temporizador / 10, 0));
            Textura = (Myrend.material.mainTexture == null) ? "OFF" : "ON";
        }
        if (Temporizador >= 60.0f)
        {
            Application.Quit();
        }
    }
    
    void OnGUI()
    {
        string seconds = Mathf.Floor(Temporizador % 60).ToString("00");
        string milis = Mathf.Floor(Temporizador * 1000 % 1000).ToString("00");
        GUI.Label(new Rect(10, 200, 350, 100), "Shader" + ":  " + Myrend.material.shader.name);
        GUI.Label(new Rect(10, 230, 350, 100),  "Color" + ":  " + (Myrend.materials[0].color));
        GUI.Label(new Rect(10, 245, 350, 100), "Textura" + " : " + Textura);
        GUI.Label(new Rect(10, 260, 350, 100), "Tiempo" + ":  " + seconds + ":" + milis);
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lampara : MonoBehaviour {

    Light Luz;
    float Temporizador;

    void Start()
    {
        Luz = GetComponent<Light>();
        Luz.enabled = !Luz.enabled;
    }
    void Update()
    {
        Temporizador += Time.deltaTime;
        if (Temporizador >= 5)
        {
            Luz.enabled = true;
        }   
    }

    void OnGUI()
    {
        if (Luz.enabled == true)
        {
            GUI.Label(new Rect(10, 215, 250, 100), "Light" + ": " + "ON");
        }
        else
        {
            GUI.Label(new Rect(10, 215, 250, 100), "Light" + ": " + "OFF");
        }
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Txt : MonoBehaviour {


    float Temp;
    Renderer Myrend;
    Shader shader03;
    private string Text;

    void Start()
    {
        //shader03 = Shader.Find("Custom/Examen_NormalMap");
    }
    void Awake()
    {
        Application.targetFrameRate = 300;
    }

    private void Update()
    {
        Temp += Time.deltaTime;

        if (Temp >= .50f)
        {
            Text = "Juan Manuel Romero Pérez A01226701";
        }
        if (Temp >= 6.0f)
        {
            Text = "";
        }

        if (Temp >= 9.0f)
        {
            Text = "Shader: Silhouette + Textured";
        }
        if (Temp >= 15.0f)
        {
            Text ="";
        }

        if (Temp >= 16.0f)
        {
            Text = "Shader: Scrolling Texture";
        }
        if (Temp >= 23.0f)
        {
            Text = "";
        }

        if (Temp >= 25.0f)
        {
            Text = "Shader: Specular";
        }
        if (Temp >= 31.0f)
        {
            Text = "";
        }

        if (Temp >= 35.0f)
        {
            Text = "Shader: Transparent";
        }
        if (Temp >= 40.0f)
        {
            Text = "";
        }

        if (Temp >= 43.0f)
        {
            Text = "Shader: Normal Map";
        }
        if (Temp >= 47.0f)
        {
            Text = "";
        }

        if (Temp >= 48.0f)
        {
            Text = "Shader: Diffuse";
        }
        if (Temp >= 53.0f)
        {
            Text = "";
        }

        if (Temp >= 57.0f)
        {
            Text = "Ambient occlusion";
        }
        if (Temp >= 60.0f)
        {
            Text = "";
        }



    }

    void OnGUI()
    {
        GUI.Label(new Rect(10, 300, 350, 100), Text);
    }
}

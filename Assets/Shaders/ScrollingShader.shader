﻿Shader "Custom/ScrollingShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ScrollXSpeed("X Scroll Speed", Range(0,10)) = 2
		_ScrollYSpeed("Y Scroll Speed",Range(0,10)) = 2

		//uitamos glossinss y Metallic

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0
		struct Input {
			float2 uv_MainTex;
		};


		//VARIABLES
		sampler2D _MainTex;
		fixed4 _Color;
		//Fixed es un float más optimizado
		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;



		UNITY_INSTANCING_CBUFFER_START(Props)

		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {
			//Obtener las UVS de la textura principal y guardarlo en la maintext
			fixed2 ScrolledUV = IN.uv_MainTex;
			//Guardar en una variable la propiedad de la velocidad multiplicada por tiempo
			fixed xScrollValue = _ScrollXSpeed * _Time;
			fixed yScrollValue = _ScrollYSpeed * _Time;
			//Se remplaza lo ue tiene ScrolledUV por x y y
			ScrolledUV += fixed2 (xScrollValue, yScrollValue);
			//half4 es para 4 posiciones también podría ser un fixed4 pero half es más optimizado
			//con tex2D le aplicas a la maintext el ScrolledUV
			half4 c = tex2D(_MainTex,ScrolledUV);
			o.Albedo = c.rgb*_Color;
			// alpha es igual al canal Alpha  dentro de C
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

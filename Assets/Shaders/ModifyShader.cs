﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyShader : MonoBehaviour
{

    Renderer myrend;
    //rendrer guarda las popiedades graficas d un objeto

    void Start()
    {
        myrend = GetComponent<Renderer>();
    }
    void Update()
    {
        float ambient = Mathf.PingPong(Time.time, 10); //(rebote infinito)
        myrend.material.SetFloat("_MySlider", ambient);
    }
}

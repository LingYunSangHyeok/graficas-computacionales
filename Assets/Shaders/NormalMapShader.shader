﻿Shader "Custom/NormalMapShader" {
	Properties {
		//Solo necesitamos Maintex
		//creamos normal map
		//bump es igual a color gris
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap ("normalmap", 2D)= "bump" {}	
		_NormalMapIntensity("Normal Intensity", Range (0,1)) = .5
		 

	}   
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		 
		CGPROGRAM
		//También se borra el Standard y se cambia a Lambert

		#pragma surface surf Lambert 
		

		
		//Agregamos otro sampler2D
		sampler2D _NormalMap;
		sampler2D _MainTex;
		float _NormalMapIntensity;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NomalMap;
		
		};

		//Borramos la parte de Standard
		void surf (Input IN, inout SurfaceOutput o)
		{
			//esta vez necesitamos solo la propiedad rgb
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			// extraer el normal map tiene  u v w


			//UnpackNormal extrae las normales y las guada en una variable float
			float3 x = UnpackNormal(tex2D(_NormalMap, IN.uv_NomalMap));
			x.x *= _NormalMapIntensity;
			x.y *= _NormalMapIntensity;
			o.Normal = normalize(x);
		}
		ENDCG
	}
	FallBack "Diffuse"
}

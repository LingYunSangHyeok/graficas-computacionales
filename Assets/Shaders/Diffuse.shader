﻿Shader "Custom/Diffuse" {
	Properties {
	//Diffuse es el shader más basico no tiene nada más ue un color
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Lambert es una tecnica más vijita ue estandar por eso borramos standar por Lambert
		#pragma surface surf Lambert fullforwardshadows
		#pragma target 3.0
		struct Input {
			float2 uv_MainTex;
		};

		//borramos el Metallicy el Glossiness
		fixed4 _Color;
		//SurfaceOutputStandard NO, solo necesitamos SourfaceOutput
		// albedo solo es el Color
		void surf (Input IN, inout SurfaceOutput o) {
		o.Albedo = _Color.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}

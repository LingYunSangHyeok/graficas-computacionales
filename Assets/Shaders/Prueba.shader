﻿Shader "Custom/Prueba" {
	Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _NormalMap ("normalmap", 2D) = "bump" {}
	  _NormalMapIntensity("Normal Intensity", Range (0,1)) = .5
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Lambert


      struct Input {
        float2 uv_MainTex;
        float2 uv_NormalMap;
		
      };
      sampler2D _MainTex;
      sampler2D _NormalMap;
	  float _NormalMapIntensity;

      void surf (Input IN, inout SurfaceOutput o) {
        o.Albedo = tex2D (_MainTex,IN.uv_MainTex).rgb;
		float3 x = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
		x.x *= _NormalMapIntensity;
		x.y *= _NormalMapIntensity;
		o.Normal = normalize(x);
        
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }



